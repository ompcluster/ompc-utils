
import argparse
import json
import glob


if __name__ == "__main__":

    ap = argparse.ArgumentParser()

    # Add the arguments to the parser
    ap.add_argument("-rt_dir", "--runtime_tracing_dir", required=True,
                    help="the directory path of JSON tracing files generated by OmpCluster")
    args = vars(ap.parse_args())

    # Get OmpTracing files
    runtime_files = glob.glob(args['runtime_tracing_dir'] + '/*.json')
    runtime_objs = None

    # Get OmpTracing JSON objects
    for file in runtime_files:
        runtime_tracing = open(file, 'r').read()
        if runtime_objs is None:
            runtime_objs = json.loads(runtime_tracing)
            lower_ts = -1
            for event in runtime_objs['traceEvents']:
                if event['ph'] == 'X':
                    if lower_ts == -1:
                        lower_ts = event['ts']
                    elif event['ts'] < lower_ts:
                        lower_ts = event['ts']
            for event in runtime_objs['traceEvents']:
                if event['ph'] == 'X':
                    event['ts'] = event['ts'] - lower_ts
        else:
            runtime_objs_new = json.loads(runtime_tracing)
            lower_ts = -1
            for event in runtime_objs_new['traceEvents']:
                if event['ph'] == 'X':
                    if lower_ts == -1:
                        lower_ts = event['ts']
                    elif event['ts'] < lower_ts:
                        lower_ts = event['ts']
            for event in runtime_objs_new['traceEvents']:
                if event['ph'] == 'X':
                    event['ts'] = event['ts'] - lower_ts
            runtime_objs['traceEvents'].extend(runtime_objs_new['traceEvents'])

    # Writing JSON timeline
    integrated_tracing = open('runtime_tracing.json', 'w')
    integrated_tracing.write(json.dumps(runtime_objs))
    integrated_tracing.close()
