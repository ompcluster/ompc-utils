#!/bin/bash
set -e

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit 1
fi

export CC=clang
export CXX=clang++
export DEFAULT_FEATURES=0
export USE_OMPCLUSTER=1
export SINGULARITYENV_CC=clang
export SINGULARITYENV_CXX=clang++
export SINGULARITYENV_DEFAULT_FEATURES=0
export SINGULARITYENV_USE_OMPCLUSTER=1

img=$1

cd task-bench
rm deps -rf
singularity exec ../sif_images/"$img".sif ./get_deps.sh
singularity exec ../sif_images/"$img".sif ./build_all.sh
cd ompcluster
mv main $img
mv main_no_datamap "$img"_no_datamap

