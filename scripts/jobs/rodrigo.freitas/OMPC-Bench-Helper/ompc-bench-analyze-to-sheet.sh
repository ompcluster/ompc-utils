set -e

bench_result="results/my_new_cool_experiment_003"

for dep in "trivial" "stencil" "fft"; do
    echo $dep
    echo
    for ccr in "\-0.5-" "\-1.0-" "\-2.0-"; do
        for iters in "10000000i" "100000000i" "200000000i"; do
            #echo "$ccr"
            #echo "$iters"
            #echo "$dep"
            ret_val=$(./ompcbench analyze "$bench_result" | grep -A 2 "$dep" | grep -A 2 "$ccr" | grep -A 2 "$iters" | sed "s/\./,/g" | sed -n 3p | cut -f 2)
            ret_val=$(echo $ret_val | sed "s/ ± /	/g")
            printf "$ret_val\t"
        done
    done
    echo
done

