FILE=osu-micro-benchmarks-5.7.tar.gz
if [ ! -f "$FILE" ]; then
    echo "$FILE does not exist. Downloading..."
    wget http://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-5.7.tar.gz
fi

rm osu-micro-benchmarks-5.6.3  -rf
tar -xf osu-micro-benchmarks-5.6.3.tar.gz
cd osu-micro-benchmarks-5.6.3
./configure CC=mpicc CXX=mpicc
make -j

sleep 3

srun --network IB -N 2 -n 2 mpirun -np 2  ./mpi/pt2pt/osu_bw
