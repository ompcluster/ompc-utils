#!/bin/bash

header() {
    echo
    echo "===================================================================="
    echo "$@"
    echo "===================================================================="
    echo
}

if (( $# != 5 )); then
    echo "Wrong usage: ./benchmark.sh <scratch> <img> <script> <source> <branch>"
    exit 1
fi

# Paths
SCRATCH=$(realpath $1)
IMAGE=$(realpath $2)
SCRIPT=$(realpath $3)
SOURCE=$(realpath $4)
BRANCH=$5
RESULTS="$SCRATCH/results"
BIN="$SCRATCH/taskbench_$BRANCH"

# Compiler and configs
export CC=clang
export CXX=clang++

header "Compiler information"
singularity exec $IMAGE \
    $CC --version
singularity exec $IMAGE \
    $CXX --version

header "Compile taskbench"
pushd $SOURCE && git checkout $BRANCH && popd
singularity exec $IMAGE bash -c "\
    make -C $SOURCE/core clean && \
    make -C $SOURCE/ompcluster clean && \
    make -C $SOURCE/core && \
    make -C $SOURCE/ompcluster"
cp $SOURCE/ompcluster/main $BIN

header "Submit for execution"
types=(
  stencil_1d
  fft
)
sizes=(
  "8 2"
  "8 4"
  "8 8"
  "8 16"
)
iters=(
  10000000
  100000000
  1000000000
)
bytes=(
  1000000
  10000000
  100000000
)

mkdir -p $RESULTS

for type in "${types[@]}"; do
for size in "${sizes[@]}"; do
for iter in "${iters[@]}"; do
for byte in "${bytes[@]}"; do

dim=($size)
res="$RESULTS/${BRANCH}_${type}_${dim[0]}_${dim[1]}_${iter}_${byte}";
echo "sbatch --output $res $SCRIPT $IMAGE $BIN $type ${dim[0]} ${dim[1]} $iter $byte;"
sbatch --output $res $SCRIPT $IMAGE $BIN $type ${dim[0]} ${dim[1]} $iter $byte;

done
done
done
done
