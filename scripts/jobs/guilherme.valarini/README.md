# Guilherme Valarini's Script

These are some of the scripts that I use when dealing with tests and benchmarks, specially at SDumont.

- [taskbench.sh](./taskbench.sh) is used with `sbatch` command to launch a job to execute a single taskbench execution. Although it can be used standalone, it was made to be called by [benchmark.sh](./benchmark.sh) with these parameters: `sbatch taskbench.sh <singularity_img> <taskbench_binary> <dep_type> <graph_width> <graph_steps/height> <#iter> <#dep_bytes>`
- [benchmark.sh](./benchmark.sh) is used as a standalone bash script to execute taskbench multiple times with different parameters.