#!/bin/bash
#SBATCH --nodes 9-9
#SBATCH --exclusive
# FOR SDUMONT
#SBATCH --partition ict_cpu
#SBATCH --account brcluster

if (( $# != 7 )); then
    echo "Wrong usage: sbatch taskbench.sh <img> <bin> <dep_type> <width> <steps> <iter> <output>"
    exit 1
fi

# Paths
IMAGE="$1"
TASKBENCH_BIN="$2"
shift 2

# Modules
module purge
# FOR SORGAN
# module load mpich/3.4.1-ucx-cuda
# module load git/2.29.0
# FOR SDUMONT
module load ucx/1.9
module load mpich/3.4_ucx-1.9

for i in {01..10}; do
    echo "$i iteration:";
    srun --mpi=pmi2 -n $SLURM_JOB_NUM_NODES singularity exec $IMAGE \
        $TASKBENCH_BIN \
        -kernel compute_bound \
        -type $1 \
        -width $2 \
        -steps $3 \
        -iter $4 \
        -output $5;
done
