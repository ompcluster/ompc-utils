#!/bin/bash

# Awave3D OmpCluster script ----------------------------------------------------
#
#   \author   Gustavo Leite <g264964@dac.unicamp.br>
#   \date     Created in January 2021.
#
# This script is intended to be used in the SDumont cluster. It basically
# configures the environment, load some modules, compile and run Awave3D with
# You can find inline commments that (tries to) explain what each thing is
# doing.
#
# Usage:
#
#   $ sbatch ./awave3d-ompcluster-sdumont.sh <dataset> <action>
#
# Allowed values for <dataset> are:
# - small, medium, large, salt, ...
#
# Allowed values for <action> are:
# - 'mod' for modeling
# - 'mig' for migration
#
# ------------------------------------------------------------------------------

# Slurm Preamble {{{1

# The following block after this comment tells Slurm which parameters we would
# like to launch our job. It look like comments, but they have special meaning
# to Slurm. They are:
#
# * account     The Slurm account, use brcluster.
# * partition   The Slurm partition, use 'ict_gpu' or 'ict_cpu'.
# * nodes:      how many nodes to request (1 in this case)
# * output:     which file to save the stdout/stderr of this script.
# * exclusive:  we don't want other people jobs running in the same machine as
#               ours. This is important if you are measuring time.
# * job-name:   name that identifies your job in the queue.
# * time:       maximum timelimit of this job. The job will be killed if it
#               exceeds 6 hours.

#SBATCH --account brcluster
#SBATCH --partition ict_gpu
#SBATCH --nodes 3
#SBATCH --output logs/ompc-%j.txt
#SBATCH --exclusive
#SBATCH --job-name a3d-ompc
#SBATCH --time 06:00:00

# Launch job {{{1

# Normally when we write sbatch scripts, we invoke them like this:
#
#   $ sbatch myscript.sh <args...>
#
# But since I don't like typing, I would like to invoke them like any other
# regular executable script. In this sense, the following if state checks if we
# are running inside slurm. If we are NOT, them we recursively invoke this
# script but with sbatch this time.
#
# For convenience, I also parse the return of the sbatch command, grab the of
# the job to my terminal. If you don't want to see the output in real time just
# press Ctrl+C.  job ID and run `tail -f <output file>` that will continuously
# print the output
#
# Note: if you execute this script with sbatch directly this if will evaluate to
# false and will not execute the `then` block.

if [[ -z "$SLURM_JOB_ID"  ]]; then
    string="$(sbatch "$0" $@)"
    sleep 1
    tail -f "logs/ompc-${string//[!0-9]/}.txt"
    exit 0
fi

# Environment {{{1

# Tells bash to stop being insane, i.e. the entire script will fail if a single
# command fail.
set -eo pipefail

# Path to where the singularity image is located, change this to where your
# image resides.
#
# TODO: Edit this!
# NOTE: This variable should point to your Singularity image.
# NOTE: The image should reside in the scratch partition.
IMAGE="/scratch/parceirosbr/brcluster/gustavo.leite/awave.sif"

if [[ -z "$IMAGE" ]]; then
    cat >&2 << EOF
Error: no Singularity image specified. Please edit this script and make the
variable \$IMAGE point to the correct location. Note: the container image should
reside in the scratch partition!
EOF
    exit 1
fi

# Purge (unload) all modules and then load only what we will need.
module purge
module load cuda/10.2
module load ucx/1.9+cuda
module load mpich/3.4_ucx-1.9+cuda

# Configure some environment variables that will be exported inside the
# container for us.
export SINGULARITYENV_CC=clang
export SINGULARITYENV_CXX=clang++
export SINGULARITYENV_AWAVE_MAX_PARALLEL_SHOT=4
export SINGULARITYENV_LIBOMPTARGET_FT_DISABLE=1
export SINGULARITYENV_LIBOMPTARGET_DEBUG=0

# Build Awave3D {{{1

# Print the compile version. This is going to help you in the future when you
# are revising your logs, so I encourage you to keep this.
singularity exec --nv $IMAGE \
    clang --version

# Build the code which resides in the `build` directory.
singularity exec --nv $IMAGE \
    make --no-print-directory -C build awave3d-ompcluster

# Run Awave3D {{{1

# Record the start time of the command
BEG=$SECONDS

# Sanity checks
[[ -z "$1" ]] && echo "Warning: dataset not specified, assuming 'small'." >&2
[[ -z "$2" ]] && echo "Warning: actiont not specified assuming 'mig'." >&2

# Select default dataset and action in case the user did not specify.
data=${1:-small}
action=${2:-mig}

# Run Awave3D with MPI inside the container.
# The variable `SLURM_JOB_NUM_NODES` is set by Slurm with the number of
# nodes requested in the preamble.
srun --mpi=pmi2 -n $SLURM_JOB_NUM_NODES \
    singularity exec --nv $IMAGE \
    build/awave3d-ompcluster -p tests/$data-$action.par

# Print a message with the time it took to execute the command
# The `==>` marker will help you search for these lines in the file.
# Grep is your friend.
echo "==> $data-$action-ompc ended in $((SECONDS-BEG)) seconds"

# }}}
