#!/bin/bash

# Awave3D OmpCluster benchmarking script ---------------------------------------
#
#   \author   Gustavo Leite <g264964@dac.unicamp.br>
#   \date     Created in January 2021.
#
# This script is intended to be used in the Sorgan cluster. It basically
# configures the environment, load some modules, compile and run Awave3D with
# many different datasets. You can find inline commments that (tries to) explain
# what each thing is doing.
#
# Usage:
#
#   $ ./awave3d-ompcluster-sorgan-bench.sh
#
# ------------------------------------------------------------------------------

# Slurm Preamble {{{1

# The following block after this comment tells Slurm which parameters we would
# like to launch our job. It look like comments, but they have special meaning
# to Slurm. They are:
#
# * output:     which file to save the stdout/stderr of this script.
# * nodes:      how many nodes to request (1 in this case)
# * exclusive:  we don't want other people jobs running in the same machine as
#               ours. This is important if you are measuring time.
# * job-name:   name that identifies your job in the queue.
# * time:       maximum timelimit of this job. The job will be killed if it
#               exceeds 6 hours.

#SBATCH --output logs/ompc-%j.txt
#SBATCH --nodes 1
#SBATCH --exclusive
#SBATCH --job-name a3d-ompc
#SBATCH --time 06:00:00

# Launch job {{{1

# Normally when we write sbatch scripts, we invoke them like this:
#
#   $ sbatch myscript.sh <args...>
#
# But since I don't like typing, I would like to invoke them like any other
# regular executable script. In this sense, the following if state checks if we
# are running inside slurm. If we are NOT, them we recursively invoke this
# script but with sbatch this time.
#
# For convenience, I also parse the return of the sbatch command, grab the of
# the job to my terminal. If you don't want to see the output in real time just
# press Ctrl+C.  job ID and run `tail -f <output file>` that will continuously
# print the output
#
# Note: if you execute this script with sbatch directly this if will evaluate to
# false and will not execute the `then` block.

if [[ -z "$SLURM_JOB_ID"  ]]; then
    string="$(sbatch "$0" $@)"
    sleep 1
    tail -f "logs/ompc-${string//[!0-9]/}.txt"
    exit 0
fi

# Environment {{{1

# Tells bash to stop being insane, i.e. the entire script will fail if a single
# command fail.
set -euo pipefail

# Path to where the singularity image is located, change this to where your
# image resides.
IMAGE="$HOME/images/awave-v1.2.1.sif"

# Purge (unload) all modules and then load only what we will need.
module purge
module load mpich-3.3.2-gcc-8.3.0-qq5ziyx
module load ucx-1.9.0-gcc-8.3.0-moszgnp
module load cuda-10.2.89-gcc-8.3.0-ehvfnbk

# Configure some environment variables that will be exported inside the
# container for us.
export SINGULARITYENV_CC=clang
export SINGULARITYENV_CXX=clang++
export SINGULARITYENV_AWAVE_MAX_PARALLEL_SHOT=4
export SINGULARITYENV_LIBOMPTARGET_FT_DISABLE=1
export SINGULARITYENV_LIBOMPTARGET_DEBUG=0

# Build Awave3D {{{1

# Print the compile version. This is going to help you in the future when you
# are revising your logs, so I encourage you to keep this.
singularity exec --nv $IMAGE \
    clang --version

# Build the code which resides in the `build` directory.
singularity exec --nv $IMAGE \
    make --no-print-directory -C build awave3d-ompcluster

# Run Awave3D {{{1

# Execute the code for each pair of (action, dataset).
# The actions are: mod (modeling); and mig (migration).
# Datasets are: small, medium, and large.
for action in mod mig; do
    for data in small medium large; do

        # Record the start time of the command
        BEG=$SECONDS

        # Run Awave3D with MPI inside the container.
        # The variable `SLURM_JOB_NUM_NODES` is set by Slurm with the number of
        # nodes requested in the preamble.
        srun --mpi=pmi2 -n $SLURM_JOB_NUM_NODES \
            singularity exec --nv $IMAGE \
            build/awave3d-ompcluster -p tests/$data-$action.par

        # Print a message with the time it took to execute the command
        # The `==>` marker will help you search for these lines in the file.
        # Grep is your friend.
        echo "==> $data-$action-ompc ended in $((SECONDS-BEG)) seconds"

    done
done

# }}}
