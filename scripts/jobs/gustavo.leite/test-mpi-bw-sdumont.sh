#!/bin/bash

# MPI Bandwidth test @ SDumont Cluster -----------------------------------------
#
#   \author   Gustavo Leite <g264964@dac.unicamp.br>
#   \date     Created in February 2021.
#
# Test the bandwidth of internode communication in the Santos Dumont cluster.
#
# Usage:
#
#   $ sbatch ./test-mpi-bw-sdumont.sh
#
# ------------------------------------------------------------------------------

# Slurm Preamble {{{1

#SBATCH --partition ict_gpu
#SBATCH --account brcluster
#SBATCH --nodes 2
#SBATCH --exclusive

# Modules {{{1

module purge
module load cuda/11.1
module load ucx/1.9+xpmem
module load mpich/3.4_ucx-1.9+cuda
# module load ucx/1.9+cuda

# Singularity image {{{1

# Attention: the image path might be different for you.
IMAGE="/scratch/parceirosbr/brcluster/$USER/awave.sif"

# Environment {{{1

# Export some variables to tell UCX we want to use Infiniband.
# I don't actually know which of the following two exports is the correct way,
# so I just export both.
#
# TODO: Test which is the correct way to export mellanox devices.
export UCX_NET_DEVICES=mlx5_0:1
export SINGULARITYENV_UCX_NET_DEVICES=mlx5_0:1

# Run OUTSIDE container {{{1

echo "==> Running OUTSIDE container"
srun --mpi=pmi2 -n 2 \
    osu/build/libexec/osu-micro-benchmarks/mpi/pt2pt/osu_bw

# Run INSIDE container {{{1

echo "==> Running INSIDE container"
srun --mpi=pmi2 -n 2 singularity exec --nv $IMAGE \
    /opt/osu/libexec/osu-micro-benchmarks/mpi/pt2pt/osu_bw

# }}}
