#!/bin/bash

# TaskBench Launcher {{{1
#
#   \author   Guilherme Valarini
#   \author   Gustavo Leite
#   \date     Created in 2021-03-30.
#
# Launches a TaskBench job. This script was created to be called by benchmark.sh
#
# Usage:
#   $ sbatch --nodes <X> taskbench.sh \
#       <samples> <dept> <width> <steps> <iters> <bytes> <ccr>
#
# SLURM {{{1

#SBATCH --exclusive
#SBATCH --account   brcluster
#SBATCH --partition ict_cpu
#SBATCH --job-name  taskbench
#SBATCH --time      01:00:00

# FUNCTIONS {{{1

function error() {
    echo "Error: $@" >&2
}

function fatal() {
    echo "Fatal: $@" >&1
    exit 10
}

function section() {
    echo "============================================================"
    echo "${1:-SECTION}"
    echo "============================================================"
    echo
}

# VARIABLES {{{1

if (( $# != 7 )); then
    fatal "wrong number of parameters"
fi

samples="$1"
dep="$2"
width="$3"
steps="$4"
iters="$5"
bytes="$6"
ccr="$7"

export LIBOMPTARGET_DEBUG=1

# OmpCluster Schedulers
schedulers=(
    "heft"
    "roundrobin"
)

# MODULES {{{1

module purge

if [[ "$(hostname)" =~ "sdumont" ]]; then
   module load cuda/11.0
   module load mpich/3.4_ucx-1.9+cuda
   module load ucx/1.9+cuda
else
   fatal "don't know which modules to load in this cluster :shrug:"
fi

# CHECKS {{{1

if [[ -z "$IMAGE" || ! -f "$IMAGE" ]]; then
    error "Either IMAGE does not exist or is not a file."
    error "  IMAGE: \"$IMAGE\""
    error "  Path:  $(realpath $IMAGE)"
    exit 1
fi

if [[ -z "$SLURM_JOB_ID" ]]; then
   fatal "Not running inside Slurm!"
fi

# INFORMATION {{{1

section "Information"

singularity exec $IMAGE clang --version

echo
echo "Samples   $samples"
echo "Dep       $dep"
echo "Width     $width"
echo "Steps     $steps"
echo "Iters     $iters"
echo "Bytes     $bytes"
echo "CCR       $ccr"
echo

# EXECUTION {{{1

section "Execution"

# This is the computation cost used by HEFT. Since the cluster can execute 200M
# iterations per second, we make an estimate by dividing the real number of
# iterations by 200M.
#
# TODO: There might be a less ugly way of doing this.
export OMPCLUSTER_HEFT_COMP_COST="$(bc <<< "scale=12; $iters/200000000")"


# This is the coefficient that will multiply the number of bytes of
# communication between two tasks. Since it was measured that the cluster if
# traansferring data at a rate of BW=1G/s, we use (1/BW) as our coefficient.
#
# The HEFT algorithm will then compute the communication cost as: bytes / BW.
# And this will produce an estimate in seconds.
#
# TODO: There might be a less ugly way of doing this.
export OMPCLUSTER_HEFT_COMM_COEF="$(bc <<< "scale=12; 1/1000000000")"

# For each scheduler
for sched in "${schedulers[@]}"; do
    export OMPCLUSTER_SCHEDULER="$sched"

    # For each sample
    for sample in $(seq $samples); do

        BEGIN=$SECONDS
        # Run taskbench
        srun --mpi=pmi2 -n $SLURM_JOB_NUM_NODES singularity exec $IMAGE \
           ./ompcluster/main \
               -kernel compute_bound \
               -type   $dep \
               -width  $width \
               -steps  $steps \
               -iter   $iters \
               -output $bytes
        ELAPSED=$(( SECONDS - BEGIN ))

        echo
        echo "==> Took $ELAPSED seconds to execute"
        echo "$sched sample=$sample dep=$dep width=$width steps=$steps iters=$iters bytes=$bytes elapsed=$ELAPSED" >> execution.log
    done
done

# }}}
