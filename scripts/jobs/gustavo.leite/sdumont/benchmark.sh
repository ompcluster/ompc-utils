#!/bin/bash

# OmpCluster Scheduler Benchmark {{{1
#
#   \author   Guilherme Valarini
#   \author   Gustavo Leite
#   \date     Created in 2021-04-06.
#
# Launch an array of TaskBench jobs with parameters of varying sizes.
#
# Usage:
#   $ ./benchmark.sh -h
#
# FUNCTIONS {{{1

function error() {
    echo "Error: $@" >&2
}

function fatal() {
    echo "Fatal: $@" >&1
    exit 10
}

function section() {
    echo "============================================================"
    echo "${1:-SECTION}"
    echo "============================================================"
    echo
}

function help() {
    cat << EOF
Usage: $0 <samples> <nodes> <container>

OPTIONS:
    <samples>       The number of samples to be collected.
    <nodes>         The number of nodes to allocate.
    <container>     The singularity container to use.
EOF
}

# SANITIZE {{{1

if [[ "$#" == 0 ]]; then
    help
    exit 0
fi

if [[ $# != 3 ]]; then
    error "missing parameters"
    help
    exit 1
fi

# PARAMETERS {{{1

# Number of samples to collect per parameter combination.
samples="$1"

if [[ "$samples" -lt 1 ]]; then
    fatal "trying to collect less than 1 sample."
fi

# Number of nodes to allocate.
nodes="$2"

if [[ "$nodes" -lt 2 ]]; then
    fatal "OmpCluster requires at least 2 nodes."
fi

# Container image.
export IMAGE="$(realpath $3)"

if [[ ! -f "$IMAGE" ]]; then
    fatal "the container image is not a file."
fi

# The dimension of the task graph as (<width> <depth>).
dimensions=(
    "8 16"      #   8x16 =  128 tasks
#   "16 32"     #  16x32 =  512 tasks
#   "16 64"     #  16x64 = 1024 tasks
#   "32 128"    # 32x128 = 4096 tasks
)

# Dependency types in TaskBench.
dependencies=(
    stencil_1d
    fft
    trivial
#   all_to_all
#   dom
#   nearest
#   no_comm
#   random_nearest
#   random_spread
#   spread
#   stencil_1d_periodic
#   tree
)

# Number of iterations each task will compute.
# - Santos Dumont throughput: 200M iters/second.
iters=(
    10000000    #  10M iterations =  50ms/task
    100000000   # 100M iterations = 500ms/task
    200000000   # 200M iterations =   1 s/task
)

# Communication to computation ratios.
# - Santos Dumont BW: 1.0 GB/s
#
# For example:
#
#          +--------------------+
#          |        CCR         |
#   +------+------+------+------+
#   | Iter |  0.5 |  1.0 |  2.0 |
#   +------+------+------+------+
#   |  10M |  25M |  50M | 100M |
#   | 100M | 250M | 500M | 1.0G |
#   | 200M | 500M | 1.0G | 2.0G |
#   +------+------+------+------+
CCRs=(
    0.5     # Communication take half as much time
    1.0     # Communication take the same amount of time
    2.0     # Communication take double the amount of time
)

# Where the output data will be saved
OUTPUTDIR="bench-$(date +%Y-%m-%d)"

# INFORMATION {{{1

section "Information"

info=$(singularity exec --nv $IMAGE clang --version | head -1)
echo "Container  $(realpath $IMAGE)"
echo "Compiler   $(echo $info | perl -n -e '/^(.+)\(/ && print $1')"
echo "Commit     $(echo $info | perl -n -e '/\(.+ (\w+)\)/ && print $1')"
echo "Samples    $samples"
echo "Nodes      $nodes (1 head, $((nodes-1)) worker(s))"
echo

# LAUNCH {{{1

section "Launch"

mkdir -p "$OUTPUTDIR"

for dept in "${dependencies[@]}"; do
for dims in "${dimensions[@]}"; do
for iter in "${iters[@]}"; do
for CCR in "${CCRs[@]}"; do

    dim=($dims)
    out="$OUTPUTDIR/tb-$dept-i$iter-CCR$CCR-${dim[0]}x${dim[1]}.out"
    err="$OUTPUTDIR/tb-$dept-i$iter-CCR$CCR-${dim[0]}x${dim[1]}.err"
    byte=$(bc <<< "$iter * 5 * $CCR")

    # Launch Slurm job
    sbatch --output "$out" --error "$err" --nodes "$nodes-$nodes" \
        ./taskbench.sh $samples $dept ${dim[0]} ${dim[1]} $iter $byte $CCR

done
done
done
done

#}}}
