Gustavo Leite's scripts
================================================================================

> **Note**: here be dragons, proceed carefully.

This is a list of some of the scripts I use day-to-day:

- Benchmarking Awave3D OmpCluster @ Sorgan ([script][1]).
- Test MPI internode bandwidth with OSU @ SDumont ([script][2]).
- Run Awave3D OmpCluster once @ Sdumont ([script][3])

[1]: awave3d-ompcluster-sorgan-bench.sh
[2]: test-mpi-bw-sdumont.sh
[3]: awave3d-ompcluster-sdumont.sh
