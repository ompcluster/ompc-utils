# Clang builds enviroment variables
# ==============================================================================
function choose_clang() {
  if (( $# > 0 )); then
    SELECTED_CLANG="$1";
    if ls "${SELECTED_CLANG}/bin/clang" >& /dev/null; then
      CLANG_INCLUDE_PATH=( ${SELECTED_CLANG}/lib/clang/*/include )
      export PATH=${SELECTED_CLANG}/bin:$PATH;
      export LD_LIBRARY_PATH=${SELECTED_CLANG}/lib:${SELECTED_CLANG}/projects/openmp/libomptarget/:$LD_LIBRARY_PATH;
      export LIBRARY_PATH=${SELECTED_CLANG}/lib:${SELECTED_CLANG}/projects/openmp/libomptarget/:$LIBRARY_PATH;
      export CPATH=${SELECTED_CLANG}/projects/openmp/runtime/src:$CLANG_INCLUDE_PATH:$CPATH;
      return 0;
    else
      echo "Selected clang not found.";
    fi
  fi
